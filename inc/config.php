<?php

    return (object) array(
        'user' => 'homestead',
        'pass' => 'secret',
        'dsn' => 'mysql:host=localhost;dbname=homestead;port=33060;',
        'opt' => array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
    )
?>
