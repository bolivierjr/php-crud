<?php
    require('Guests.class.php');

    class Crud {
        private $dbh;

        public function __construct($dbh) {
            $this->dbh = $dbh;
        }

        public function __destruct() {
            //$this->dbh = null;
        }

        public function create($params) {
            $sql = 'INSERT INTO MyGuests (firstname,lastname,email)
                            VALUES (:firstname,:lastname,:email)';

            $stmt = $this->dbh->prepare($sql);
            $stmt->execute($params);
        }

        public function read() {
            $sql = 'SELECT firstname,lastname FROM MyGuests';

            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_CLASS, 'Guests');
        }

        private function getGuests() {

        }

    }

?>
