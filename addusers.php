<?php
    require('db_connect.php');

    $params = array(
        ':firstname' => $_POST['firstname'],
        ':lastname' => $_POST['lastname'],
        ':email' => $_POST['email']
    );

    $crud->create($params);
    header('Location: http://homestead.test/');
?>
