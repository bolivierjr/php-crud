<?php
    require('classes/Crud.class.php');

    $config = require('inc/config.php');

    try {
        $dbh = new PDO(
            $config->dsn,
            $config->user,
            $config->pass,
            $config->opt
        );
    } catch(PDOException $e) {
        error_log("Error!: " .$e->getMessage(). "<br>");
        exit();
    }

    $crud = new Crud($dbh);
?>
